package com.example.mylibraryagain;

public class MyCalculator {

    public int add(int num1, int num2) {
        return num1 + num2;
    }

    public int subtract(int num1, int num2) {
        return num1 - num2;
    }

    public int multiply(int num1, int num2) {
        return num1 * num2;
    }

    // Handling division by zero by returning Integer.MIN_VALUE as a convention
    public int divide(int num1, int num2) {
        if (num2 == 0) {
            return Integer.MIN_VALUE;
        }
        return num1 / num2;
    }

    // Additional function for modulus operation
    public int modulus(int num1, int num2) {
        if (num2 == 0) {
            return Integer.MIN_VALUE;
        }
        return num1 % num2;
    }
}
